using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

namespace ImageToPdf
{
    public partial class MainForm : Form
    {
        bool success = false;
        DataTable dt;
        public MainForm()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                string resourceName = new AssemblyName(args.Name).Name + ".dll";
                string resource = Array.Find(this.GetType().Assembly.GetManifestResourceNames(), element => element.EndsWith(resourceName));

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };
            this.Icon = Properties.Resources.application_pdf_36278;
            InitializeComponent();
            dt = new DataTable();
            dt.Columns.Add("LinkGoc");
            dt.Columns.Add("LinkXuat");
            dataGridView1.DataSource = dt;
        }

        private void btnSelectSrc_Click(object sender, EventArgs e)
        {
            ofdSrcFile.Multiselect = true;
            if (ofdSrcFile.ShowDialog() != DialogResult.OK)
                return;
            foreach (string file in ofdSrcFile.FileNames)
            {
                DataRow dr = dt.NewRow();
                dr["LinkGoc"] = file;
                dr["LinkXuat"] = Path.GetDirectoryName(file) + "\\" +
                    Path.GetFileNameWithoutExtension(file) + ".pdf";
                dt.Rows.Add(dr);

            }
        }



        private void btnConvert_Click(object sender, EventArgs e)
        {
            errProv.Clear();
            if (dataGridView1.RowCount <= 0)
            {
                MessageBox.Show("không có file để chuyển đổi");
                return;
            }
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(bw_DoWork);
            worker.RunWorkerAsync();

            MessageBox.Show("Chuyển đổi hoàn tất.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                for (int i=0; i< dataGridView1.RowCount;i++)
                {
                string source = dataGridView1.Rows[i].Cells[0].Value.ToString();
                string destinaton = dataGridView1.Rows[i].Cells[1].Value.ToString();
        

                PdfDocument doc = new PdfDocument();
                XSize size = new XSize(0, 0);

               
                    
                    // each source on separate page
                    var page = new PdfPage();
                    page.Size = PageSize.A4;
                    doc.Pages.Add(page);
                    XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
                    XImage img = XImage.FromFile(source);
                    XUnit height = XUnit.FromInch(img.PixelHeight / img.VerticalResolution);
                    XUnit width = XUnit.FromInch(img.PixelWidth / img.HorizontalResolution);

                  

                    page.Height = height;
                    page.Width = width;
                    xgr.DrawImage(img,0,0);

                    img.Dispose();
                    xgr.Dispose();


              
                    doc.Save(destinaton);
                doc.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {


            if (success)
                MessageBox.Show("The converion ended successfully.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            dt.Clear();
            dataGridView1.DataSource = dt;

        }


    }
}